const perguntas = [
    {
        pergunta: "Qual é a maneira correta de comentar uma linha de código em JavaScript?",
        respostas: [
            "// Comentário",
            "/* Comentário */",
            "' Comentário"
        ],
        correta: 0
    },
    {
        pergunta: "Qual destas é uma estrutura de controle de fluxo em JavaScript?",
        respostas: [
            "if-else",
            "loop",
            "switch-case"
        ],
        correta: 0
    },
    {
        pergunta: "O que o método 'console.log()' faz em JavaScript?",
        respostas: [
            "Exibe um diálogo na tela",
            "Registra mensagens no console do navegador",
            "Altera o estilo de um elemento HTML"
        ],
        correta: 1
    },
    {
        pergunta: "O que é uma variável em JavaScript?",
        respostas: [
            "Um valor constante que não pode ser alterado",
            "Um contêiner para armazenar dados",
            "Uma função que executa uma tarefa específica"
        ],
        correta: 1
    },
    {
        pergunta: "Qual é o operador de igualdade estrita em JavaScript?",
        respostas: [
            "==",
            "===",
            "!="
        ],
        correta: 1
    },
    {
        pergunta: "Como você declara uma função em JavaScript?",
        respostas: [
            "função minhaFuncao() {}",
            "declare minhaFuncao() {}",
            "function minhaFuncao() {}"
        ],
        correta: 2
    },
    {
        pergunta: "O que o método 'querySelector()' faz em JavaScript?",
        respostas: [
            "Seleciona um elemento do HTML pelo seu ID",
            "Seleciona um elemento do HTML pelo seu nome",
            "Seleciona um elemento do HTML usando um seletor CSS"
        ],
        correta: 2
    },
    {
        pergunta: "Qual é a função do operador 'typeof' em JavaScript?",
        respostas: [
            "Retorna o tipo de dado de uma variável",
            "Verifica se uma variável é definida ou não",
            "Converte uma string em um número"
        ],
        correta: 0
    },
    {
        pergunta: "O que é uma 'closure' em JavaScript?",
        respostas: [
            "Uma função que não tem acesso ao escopo externo",
            "Uma função que tem acesso ao escopo externo, mesmo após a função ter sido executada",
            "Uma função que só pode ser chamada dentro de outra função"
        ],
        correta: 1
    },
    {
        pergunta: "Qual é a diferença entre '==' e '===' em JavaScript?",
        respostas: [
            "'==' compara os valores e os tipos, enquanto '===' compara apenas os valores",
            "'==' compara apenas os valores, enquanto '===' compara os valores e os tipos",
            "'==' e '===' são iguais, ambos comparam os valores e os tipos"
        ],
        correta: 1
    }
];

const quiz = document.querySelector('#quiz')
const template = document.querySelector('template')

const corretas = new Set()
const totalDePerguntas = perguntas.length
const mostrarTotal = document.querySelector('#acertos span')
mostrarTotal.textContent = corretas.size + ' de ' + totalDePerguntas

for (const item of perguntas) {
    const quizItem = template.content.cloneNode(true)
    quizItem.querySelector('h3').textContent = item.pergunta

    for (resposta of item.respostas) {
        const dt = quizItem.querySelector('dl dt').cloneNode(true)
        dt.querySelector('span').textContent = resposta
        dt.querySelector('input').setAttribute('name', 'pergunta-' + perguntas.indexOf(item))
        dt.querySelector('input').value = item.respostas.indexOf(resposta)
        dt.querySelector('input').onchange = (event) => {
            const estaCorreta = event.target.value == item.correta

            corretas.delete(item)
            if (estaCorreta) {
                corretas.add(item)
            }

            mostrarTotal.textContent = corretas.size + ' de ' + totalDePerguntas
        }

        quizItem.querySelector('dl').appendChild(dt)
    }

    quizItem.querySelector('dl dt').remove()

    quiz.appendChild(quizItem)
}