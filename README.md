## Quiz JavaScript
Projeto construído durante o evento NLW Expert da [Rockerseat](https://www.rocketseat.com.br/) (Trilha HTML+CSS+JS) em ***Fevereiro/2024***.

Para mostrar os fundamentos da programação, neste projeto foi desenvolvido uma página HTML+CSS+JS, com um conteúdo Quiz JavaScript, em que ao selecionar a resposta correta, o total de acertos é mostrado no rodapé da página.

> HTML | CSS | JavaScript | GitLab Pages


Acesse a página do quiz: [Quiz JavaScript](https://falsilva.gitlab.io/nlw-experts-quiz-javascript/)